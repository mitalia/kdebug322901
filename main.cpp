#include <QApplication>
#include <QThread>
#include <QStringList>

#include "worker.hpp"
#include <QDebug>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    bool workaround=false;
    if(a.arguments().length()>1 && a.arguments()[1]=="workaround")
        workaround=true;

    Worker w(workaround);
    QThread t;
    w.moveToThread(&t);
    t.connect(&t, SIGNAL(started()), &w, SLOT(work()));
    t.connect(&w, SIGNAL(finished()), &t, SLOT(quit()));
    t.connect(&t, SIGNAL(finished()), &a, SLOT(quit()));
    t.start();
    a.exec();
    return 0;
}
