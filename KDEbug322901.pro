#-------------------------------------------------
#
# Project created by QtCreator 2013-07-30T01:59:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KDEbug322901
TEMPLATE = app


SOURCES += main.cpp \
    worker.cpp

HEADERS  += \
    worker.hpp

FORMS    +=
