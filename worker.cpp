#include "worker.hpp"
#include <QFileIconProvider>
#include <QFileInfo>

Worker::Worker(bool Workaround)
{
    if(Workaround)
       getIcon();
}

void Worker::getIcon()
{
    QFileIconProvider p;
    p.icon(QFileInfo("/"));
}

void Worker::work()
{
    getIcon();
    emit finished();
}
