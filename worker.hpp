#ifndef WORKER_HPP
#define WORKER_HPP

#include <QObject>

class Worker : public QObject
{
    Q_OBJECT
public:
    explicit Worker(bool Workaround);

    void getIcon();
    
public slots:
    void work();
signals:
    void finished();
};

#endif // WORKER_HPP
